const { Pool } = require('pg');

const {
  DB_HOST,
  DB_USER,
  DB_PASSWORD,
  DB_NAME,
  DB_PORT,
} = process.env;

const pool = new Pool({
  host: DB_HOST || '67.205.143.180',
  user: DB_USER || 'modulo4',
  password: DB_PASSWORD || 'modulo4',
  database: DB_NAME || 'tcs2',
  port: DB_PORT || 5432,
});

const query = (queryStr, params) => {
  return new Promise((resolve, reject) => {
    pool.query(queryStr, params, (err, res) => {
      if (err) reject(err);
      resolve(res);
    });
  });
};

module.exports = {
  query,
};
