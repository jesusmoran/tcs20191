var express = require('express');
var router = express.Router();
const db = require('../db');
const personModel = require('../models/person.model');
const programModel = require('../models/program.model');
const formacionModel = require('../models/formacion.model');
const serviciosModel = require('../models/servicios.model');

router.get('/ping', async (req, res, next) => {
  const response = await db.query('SELECT * FROM alumno_programa LIMIT 1');
  res.send(response.rows);
});

router.post('/login', async (req, res, next) => {
  const { code } = req.body;
  if (!code) res.status(400).end();
  const rows = await personModel.getPersonByCode(code);
  if (rows.length > 0) res.send(rows[0]);
  else res.status(400).end();
});

router.post('/register', async (req, res, next) => {
  const { person } = req.body;
  if (!person) res.status(400).end();
  const rows = await personModel.savePerson(person);
  res.send(rows);
});

router.post('/update', async (req, res, next) => {
  const { person } = req.body;
  if (!person) res.status(400).end();
  const response = await personModel.updatePerson(person);
  res.send(response);
});

router.post('/laboral', (req, res, next) => {
  res.end();
});

router.post('/academica', (req, res, next) => {
  const { formacion } = req.body;
  if (!formacion) res.status(400).end();
  const response = formacionModel.save(formacion);
  res.send(response);
});

router.post('/postgrado', (req, res, next) => {
  res.end();
});

router.post('/servicios', async (req, res, next) => {
  const { responses, formacionId } = req.body;
  if (!responses) res.status(400).end();

  try {
    await serviciosModel.saveResponse(formacionId, {
      service: 1,
      value: responses.formAcad,
    });
    await serviciosModel.saveResponse(formacionId, {
      service: 2,
      value: responses.calDoc,
    });
    await serviciosModel.saveResponse(formacionId, {
      service: 3,
      value: responses.sisBiblio,
    });
    await serviciosModel.saveResponse(formacionId, {
      service: 4,
      value: responses.difInvest,
    });
    await serviciosModel.saveResponse(formacionId, {
      service: 5,
      value: responses.infra,
    });
    await serviciosModel.saveResponse(formacionId, {
      service: 6,
      value: responses.insLab,
    });
    await serviciosModel.saveResponse(formacionId, {
      service: 7,
      value: responses.sectorPub,
    });
    await serviciosModel.saveResponse(formacionId, {
      service: 8,
      value: responses.sectorPriv,
    });
    await serviciosModel.saveResponse(formacionId, {
      service: 9,
      value: responses.formInt,
    });
    await serviciosModel.saveResponse(formacionId, {
      service: 10,
      value: responses.bienUniv,
    });
    await serviciosModel.saveResponse(formacionId, {
      service: 11,
      value: responses.instInt,
    });
    await serviciosModel.saveResponse(formacionId, {
      service: 12,
      value: responses.movilidad,
    });
    res.status(200).end();
  } catch (error) {
    console.error(error);
    res.status(500).end();
  }
});

router.get('/programs', async (req, res, next) => {
  const programs = await programModel.getPrograms();
  res.send(programs);
});

router.get('/academica/:code', async (req, res, next) => {
  const { code } = req.params;
  if (!code) res.status(400).end();
  const rows = await formacionModel.getByCode(code);
  res.send(rows);
});

module.exports = router;
