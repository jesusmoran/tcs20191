const db = require('../db');

const savePerson = async (person) => {
  const queryStr = `INSERT INTO public.alumno_programa
  (cod_alumno, ape_paterno, ape_materno, nom_alumno, cod_especialidad, cod_tip_ingreso,
    cod_situ, cod_perm, anio_ingreso, dni_m, id_programa, correo, correo_personal, telefono_movil,
    telefono, nac_fecha)
  VALUES($1, $2, $3, $4, 1, 'ER', 'R', 'INAC', '2010', $5, 2, $6, $7, $8, $9, $10);
  `;
  const params = [person.code, person.lastName1, person.lastName2, person.name, person.document, person.email, person.email2, person.cellphone, person.phone, person.birthday];
  const response = await db.query(queryStr, params);
  return response.rows;
};

const updatePerson = async (person) => {
  const queryStr = `UPDATE public.alumno_programa
  SET ape_paterno=$2, ape_materno=$3, nom_alumno=$4, dni_m=$5, correo=$6,
      correo_personal=$7, telefono_movil=$8, telefono=$9, nac_fecha=$10
  WHERE cod_alumno=$1;
  `;
  const params = [person.code, person.lastName1, person.lastName2, person.name, person.document, person.email, person.email2, person.cellphone, person.phone, person.birthday];
  const response = await db.query(queryStr, params);
  return response;
};

const getPersonByCode = async (code) => {
  const queryStr = 'SELECT * FROM public.alumno_programa WHERE cod_alumno = $1;';
  const params = [code];
  const response = await db.query(queryStr, params);
  return response.rows;
};

module.exports = {
  savePerson,
  updatePerson,
  getPersonByCode,
};
