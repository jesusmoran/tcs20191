const db = require('../db');

const saveLaboral = (laboral) => {
  const queryStr = `
  INSERT INTO public.experiencia_laboral
  (dedicacion_id, tipocontrato_id, motivocese_id, rol_id, medioempleo_id, institucion_id,
    area_id, explaboral_fini, explaboral_ffin, explaboral_ts, explaboral_salario,
    explaboral_rformacion, explaboral_fprin, explaboral_nempleo_egreso, explaboral_tdemora_1empleo,
    explaboral_nivel_satisfaccion, explaboral_fregistro)
  VALUES($1, 0 , 0, 0, 0, 0, 0, '', '', '', 0, '', '', 0, '', 0, '');
  `;
};

module.exports = {
  saveLaboral,
};
