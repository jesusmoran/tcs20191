const db = require('../db');

const getPrograms = async () => {
  const queryStr = `SELECT id_programa, nom_programa
  FROM public.programa where vigencia_programa = true order by id_programa ASC;`;
  const response = await db.query(queryStr);
  return response.rows;
};

module.exports = {
  getPrograms,
};
