const db = require('../db');

const saveResponse = async (formacionId, response) => {
  const queryStr = `
    INSERT INTO public.servicios_universitarios_valora
    (suvaloracion_id, formacion_id, serviciouniv_id, servuniv_fregistro, estado_id)
    VALUES($1, $2, $3, NOW(), 1);
  `;
  const params = [response.value, formacionId, response.service];
  const resp = await db.query(queryStr, params);
  return resp;
};

module.exports = {
  saveResponse,
};
