const db = require('../db');

const save = async (formacion) => {
  const queryStr = `
  INSERT INTO public.formacion
  (modalidad_id, persona_id, id_programa, nivel_id, formacion_calumno, formacion_merito, formacion_nhoraslectivas)
  VALUES(0, 1, $1, 1, $2, 0, 0);
  `;
  const params = [formacion.programId, formacion.code];
  const response = await db.query(queryStr, params);
  return response;
};

const getByCode = async (code) => {
  const queryStr = `SELECT * FROM formacion WHERE formacion_calumno=$1;`;
  const params = [code];
  const response = await db.query(queryStr, params);
  return response.rows;
};

module.exports = {
  save,
  getByCode,
};
