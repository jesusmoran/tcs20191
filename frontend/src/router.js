import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      component: () => import('./views/HomeStack.vue'),
      children: [
        { path: '', component: () => import('./views/Login.vue') },
        { path: 'register', component: () => import('./views/Register.vue') },
      ],
    },
    {
      path: '/dashboard',
      component: () => import('./views/Dashboard.vue'),
      children: [
        { path: '', component: () => import('./views/Profile.vue') },
        { path: 'update', component: () => import('./views/Update.vue') },
        { path: 'academict', component: () => import('./views/FormacionAcademica.vue') },
        { path: 'work', component: () => import('./views/ExperienciaLaboral.vue') },
        { path: 'academicv', component: () => import('./views/ValorarAcademica.vue') },
        { path: 'services', component: () => import('./views/ValorarServicios.vue') },
      ],
    },
    {
      path: '*',
      redirect: '/',
    },
  ],
});
